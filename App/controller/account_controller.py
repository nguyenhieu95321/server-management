from flask import request

from App.model.accounts import Account
from common import code_return
from common.common_utils import check_params, response_success, response_failed, debug
from common.values_const import USERNAME, PASSWORD, BIRTHDAY, PHONE, EMAIL, ROLE, ID, FULLNAME, OLD_PASSWORD, \
    NEW_PASSWORD
from config.config import db


def create_account():
    data_request = request.get_json()
    fullname = data_request[FULLNAME]
    username = data_request[USERNAME]
    password = data_request[PASSWORD]
    birthday = data_request[BIRTHDAY]
    phone = data_request[PHONE]
    email = data_request[EMAIL]
    role = data_request[ROLE]

    result_param = check_params(username, fullname, password, birthday, phone, email, role)
    if result_param != code_return.RETURN_TRUE:
        return result_param

    account_name = Account.query.filter_by(username=username).first()
    if account_name is None:
        account = Account(
            id=0,
            fullname=fullname,
            username=username,
            birthday=birthday,
            password=password,
            phone=phone,
            role=role,
            email=email
        )
        if account.insert():
            return response_success("Tạo thành công")
    return response_failed(code_return.RESPONSE_CODE_400, "Tài khoản đã tồn tại")


def get_all_account():
    result_account = Account.query.all()
    acc_list = []
    for account in result_account:
        acc = {
            ID: account.id,
            ROLE: account.role,
            FULLNAME: account.fullname,
            USERNAME: account.username,
            PHONE: account.phone,
            EMAIL: account.email,
            BIRTHDAY: account.birthday
        }
        acc_list.append(acc)
    return response_success(result=acc_list)


def get_account_by_id():
    id_account = request.args.get("id")
    if check_params(id_account):
        result_account = Account.query.filter_by(id=id_account).first()
        if result_account:
            acc = {
                ID: result_account.id,
                ROLE: result_account.role,
                FULLNAME: result_account.fullname,
                USERNAME: result_account.username,
                PHONE: result_account.phone,
                EMAIL: result_account.email,
                BIRTHDAY: result_account.birthday
            }
            return response_success(result=acc)
        return response_success(result="id không tồn tại")
    return response_success(result="id")


def update_account():
    data_request = request.get_json()
    id_account = data_request[ID]
    fullname = data_request[FULLNAME]
    birthday = data_request[BIRTHDAY]
    phone = data_request[PHONE]
    email = data_request[EMAIL]
    role = data_request[ROLE]
    account = Account.query.filter_by(id=id_account).first()
    account.phone = phone
    account.fullname = fullname
    account.birthday = birthday
    account.email = email
    account.role = role
    if account.update():
        return "ok"
    return "not ok"


def change_password():
    data_request = request.get_json()
    id_account = data_request[ID]
    old_password = data_request[OLD_PASSWORD]
    new_password = data_request[NEW_PASSWORD]

    account = Account.query.filter_by(id=id_account).first()
    if account:
        if (account.password == old_password):
            account.password = new_password
            if account.update():
                return response_success("Đổi mật khẩu thành công")
            return response_failed(code_return.RESPONSE_CODE_401, "Đổi mật khẩu thất bại")
        return response_failed(code_return.RESPONSE_CODE_401, "Mật khẩu cũ không đúng")
    return response_failed(code_return.RESPONSE_CODE_401, "Không tìm thấy tài khoản")


def delete_account():
    id_account = request.args.get("id")
    if Account.query.filter_by(id=id_account).delete():
        db.session.commit()
        return "ok"
    return "no"
