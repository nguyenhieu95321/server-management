from flask import request

from App.model.accounts import Account
from common import code_return
from common.common_utils import check_params, response_success, response_failed, debug
from common.values_const import USERNAME, PASSWORD, BIRTHDAY, PHONE, EMAIL, ROLE, ID, FULLNAME, OLD_PASSWORD, \
    NEW_PASSWORD


def login():
    data_request = request.get_json()
    username = data_request[USERNAME]
    password = data_request[PASSWORD]

    result_body = check_params(username, password)
    if result_body != code_return.RETURN_TRUE:
        return result_body

    account = Account.query.filter_by(username=username, password=password).first()
    if account:
        return response_success(result={
            ID: account.id,
            ROLE: account.role,
            FULLNAME: account.fullname,
            USERNAME: account.username,
            PHONE: account.phone,
            EMAIL: account.email,
            BIRTHDAY: account.birthday
        })
    return response_failed(code_return.RESPONSE_CODE_401, "Đăng nhập không thành công")
