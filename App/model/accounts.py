from numpy import number
from sqlalchemy import Integer, String, BINARY, Date
from sqlalchemy.sql.functions import user

from config.config import db, app
from common.model_global import Model


class Account(Model, db.Model):
    __tablename__ = "accounts"
    id = db.Column(Integer, primary_key=True, autoincrement=True)
    fullname = db.Column(String(100), nullable=False)
    username = db.Column(String(100), nullable=False)
    password = db.Column(String(100), nullable=False)
    birthday = db.Column(String(100), nullable=False)
    phone = db.Column(String(100), nullable=False)
    email = db.Column(String(100), nullable=False)
    role = db.Column(String(100), nullable=False)

    def __init__(self, id, username, fullname, password, birthday, phone, email, role):
        Model.__init__(object)
        self.id = id
        self.username = username
        self.fullname = fullname
        self.password = password
        self.birthday = birthday
        self.phone = phone
        self.email = email
        self.role = role


with app.app_context():
    db.create_all()
