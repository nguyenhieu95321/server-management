from router import app_router
from router import author_router
import config
from flask_cors import CORS
import threading

FLASK_APP = config.app
FLASK_APP.register_blueprint(app_router.account)
FLASK_APP.register_blueprint(author_router.author)

if __name__ == '__main__':
    thread_flask = threading.Thread(target=FLASK_APP.run(port=5000), args=())
    thread_flask.start()
