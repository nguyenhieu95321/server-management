import json

from flask import Flask
from urllib.parse import quote
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

f = open('./database.json')
data = json.load(f)
host = data.get('host')
user = data.get('user')
pw = data.get('pass')
db_name = data.get('database')

# host = "localhost"
# user = "root"
# pw = "21102001"
# db_name = "server_management"
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://" + user + ":" + quote(pw) + "@" + host + "/" + db_name
app.config['SQLALCHEMY_POOL_SIZE'] = 20
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 120
app.config['SQLALCHEMY_MAX_OVERFLOW'] = 10

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
