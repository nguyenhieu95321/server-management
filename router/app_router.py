from flask import request, Blueprint
from App.controller import account_controller

account = Blueprint('accounts_routes', __name__)


@account.route("/", methods=['GET'])
def test_db():
    return "OK DB"


@account.route("/test/connect", methods=['GET'])
def test():
    return "OK"


@account.route("/account/add", methods=['POST'])
def add_account():
    return account_controller.create_account()


@account.route("/account/get-all", methods=['GET'])
def get_all_account():
    return account_controller.get_all_account()


@account.route("/account/get-by-id", methods=['GET'])
def get_account_by_id():
    return account_controller.get_account_by_id()


@account.route("/account/update", methods=['PUT'])
def update_account():
    return account_controller.update_account()


@account.route("/account/change-password", methods=['PUT'])
def change_password_account():
    return account_controller.change_password()


@account.route("/account/delete", methods=['DELETE'])
def delete_account():
    return account_controller.delete_account()
