from flask import request, Blueprint
from App.controller import author_controller

author = Blueprint('author_router', __name__)


@author.route("/login", methods=['POST'])
def login():
    return author_controller.login()
