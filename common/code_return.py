CODE_EMPTY = 1001
CODE_NULL = 1005
CODE_SUCCESS = 200

RETURN_EMPTY = "Params is empty"  # Kết quả trả về rỗng (client gửi data lên thiếu dữ liệu )
RETURN_NONE = "Params is null"

HEADER_NONE = "Header is null"
HEADER_EMPTY = "Header is empty"

RETURN_PASSWORD_INCORRECT = "Password is incorrect"

# Kết quả trả về rỗng (không insert được dữ liệu vào db)
DATA_EMPTY = "Data is empty"

# Kết quả trả về là True
RETURN_TRUE = "True"
RETURN_FALSE = "False"

RESPONSE_CODE_200 = 200  # thành công
RESPONSE_CODE_201 = 201  # thành công và dữ liệu đã được tạo ra
RESPONSE_CODE_204 = 204  # Empty Response
RESPONSE_CODE_400 = 400  # Bad Request - Máy chủ không thể hiểu yêu cầu do cú pháp không hợp lệ.
RESPONSE_CODE_401 = 401  # Unauthorized
RESPONSE_CODE_403 = 403  # For Forbidden Access denied. - client khong co quyen truy cap vao noi dung
RESPONSE_CODE_404 = 404  # Data Not Found.
RESPONSE_CODE_405 = 405  # Method không được phép thực hiện hoặc không được hỗ trợ.
RESPONSE_CODE_500 = 500  # Loi he thong.
RESPONSE_CODE_503 = 503  # Service Unavailable - may chu ngung hoat dong hoac dang bao tri
