import time
import json
from common.values_const import SUCCESS, RESULT, CODE, MSG, HEADER_T
from common import code_return
import datetime
import unidecode

FLAG_DEBUG = True


def debug(*values):
    if FLAG_DEBUG:
        log = ""
        for value in values:
            log = log + ": " + str(value)
        print("Hieu " + log)


def check_params(*param):
    for p in param:
        if p is None:
            return response_failed(code_return.CODE_NULL, code_return.RETURN_NONE)
        if not p:
            return response_failed(code_return.CODE_EMPTY, code_return.RETURN_EMPTY)
    return code_return.RETURN_TRUE


def check_header(*param):
    for p in param:
        if p is None:
            return response_failed(code_return.HEADER_EMPTY, code_return.HEADER_NONE)
        if not p:
            return response_failed(code_return.HEADER_EMPTY, code_return.HEADER_EMPTY)
    return code_return.RETURN_TRUE


def response_success(result):
    t = str(time.time())
    form_return = {
        SUCCESS: True,
        RESULT: result,
        HEADER_T: t
    }
    return json.dumps(form_return)


def response_failed(code, result):
    t = str(round(time.time()) * 1000)
    form_return = {
        CODE: str(code),
        SUCCESS: False,
        MSG: result,
        HEADER_T: t
    }
    return json.dumps(form_return)
