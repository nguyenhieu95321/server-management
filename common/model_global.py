from config import db
from common.common_utils import debug


class Model(object):
    # tạo hàm insert_database
    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()
            db.session.close()
            return True
        except Exception as ex:
            print(ex)
            return False

    def update(self):
        try:
            db.session.merge(self)
            db.session.commit()
            db.session.close()
            return True
        except Exception as ex:
            self.db.session.rollback()
            debug(ex)
            return False

    def delete(self):
        try:
            self.db.session.delete(self)
            self.db.session.commit()
            db.session.close()
            return True
        except Exception as ex:
            self.db.session.rollback()
            debug(ex)
            return False
