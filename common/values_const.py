import os

CODE = "code"
SUCCESS = "success"
RESULT = "result"
HEADER_T = "t"
MSG = "msg"

NAME = "name"
ID = 'id'

USERNAME = "username"
PASSWORD = "password"
BIRTHDAY = "birthday"
PHONE = "phone"
EMAIL = "email"
ROLE = 'role'
FULLNAME = 'fullname'
OLD_PASSWORD = 'old_password'
NEW_PASSWORD = 'new_password'
